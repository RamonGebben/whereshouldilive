require([
    'maps/googleMaps',
    'maps/leafletMaps'
    ],
    function(
        GeoLib,
        MapsLib){

        GeoLib.getGeoLib().init();
        MapsLib.getMapsLib().init();

        // -----
        // Testing GeoCode
        // -----
        QUnit.asyncTest( "Geocode OK", function( assert ) {

            var testUtrecht = function(results){
                QUnit.start();

                // We know the Geographical Location of Utrecht.
                assert.ok(52.091667 - results[0].geometry.location.lat() < 0.01);
                assert.ok(5.117778 - results[0].geometry.location.lng() < 0.01);
            };

            // Testing Utrecht can be found using GMaps
            GeoLib.getGeoLib().geocode("Utrecht, The Netherlands", testUtrecht);
        });

        QUnit.asyncTest( "Geocode Error", function( assert ) {
                // We shouldnt find a result
                var testFoundResult = function(results){
                    QUnit.start();
                    assert.ok( false, "true succeeds" );
                };

                var testNoResult = function(results){
                    QUnit.start();
                    assert.ok( true, "true succeeds" );
                };

            // Testing Utrecht can be found using GMaps
            GeoLib.getGeoLib().geocode("Unknown Territory in front of you", testFoundResult, testNoResult);
        });

        // -----
        // Testing codeAddress
        // -----
        // QUnit.asyncTest( "codeAddress", function( assert ) {

        //     // Testing Utrecht can be found using GMaps
        //     GeoLib.getGeoLib().codeAddress("Utrecht, The Netherlands");
        //     var center = MapsLib.getMapsLib().getCenter();

        //     assert.ok(52.0802 - center.lat < 0.01);
        //     assert.ok(5.1028 - center.lng < 0.01);
        // });
    });