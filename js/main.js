require.config({
    paths: {
        'async': '/bower_components/requirejs-plugins/src/async',
        'text': '/bower_components/requirejs-text/text'
    }
});

require([
        'maps/leafletMaps',
        'maps/googleMaps',
        'utils/colors',
        'utils/markers',
        'utils/animate',
        'backbone/models/where_coordinate',
        'backbone/models/area_properties',
        'backbone/models/location_type',
        'backbone/models/custom_location',
        'backbone/collections/location_types',
        'backbone/collections/custom_locations',
        'backbone/views/current_coordinate',
        'backbone/views/location_types',
        'backbone/views/custom_locations'
//        'async!https://maps.googleapis.com/maps/api/js?key=AIzaSyAF6mxhlF31Au16sEVpfuPZZxLHEEkosLo&libraries=places'
    ],
    function(MapsLib,
             GeoLib,
             Colors,
             Markers,
             Animate,
             WhereCoordinate,
             AreaProperties,
             LocationType,
             CustomLocation,
             LocationTypes,
             CustomLocations,
             CurrentCoordinatesView,
             LocationTypesView,
             CustomLocationsView){


        var customLocations;
        /**
         Function that is run as soon as the window is fully loaded.
         Initializes the map and places the default starting point
         */
        $(document).ready(function (){

            //Activates toggle effects
            Animate.getAnimate().activateToogle();

            var colors = new Colors();
            var m = new Markers();

            MapsLib.getMapsLib().init(); //FIXME : This should be run automatically
            GeoLib.getGeoLib().init(); //FIXME : This should be run automatically

            // Real-time coordinate indicator
            var mainCoordinate = new WhereCoordinate({latitude: '52.0802', longitude: '5.1028'});
            var current_coord_view = new CurrentCoordinatesView({ model: mainCoordinate, el: $("#coordinates_container")});

            // General Locations
            var trainStationType = new LocationType({name: "Train Stations", type: 'train_station', id: 'train_station', areaProperties: new AreaProperties({strokeColor: colors.blue, fillColor: colors.blue, icon: m.rail_station })});
            var airportType = new LocationType({name: "Airport", type: 'airport', id: 'airport', areaProperties: new AreaProperties({strokeColor: colors.aqua, fillColor: colors.aqua, icon: m.airport})});
            var embassyType = new LocationType({name: "Embassy", type: 'embassy', id: 'embassy', areaProperties: new AreaProperties({strokeColor: colors.teal, fillColor: colors.teal, icon: m.museum})});
            var barType = new LocationType({name: "Bar", type: 'bar', id: 'bar', areaProperties: new AreaProperties({strokeColor: colors.olive, fillColor: colors.olive, icon: m.bar_lounge})});
            var bakeryType = new LocationType({name: "Bakery", type: 'bakery', id: 'bakery', areaProperties: new AreaProperties({strokeColor: colors.green, fillColor: colors.green})});
            var busStationType = new LocationType({name: "Bus Stations", type: 'bus_station', id: 'bus_station', areaProperties: new AreaProperties({strokeColor: colors.lime, fillColor: colors.lime})});
            var churchType = new LocationType({name: "Church", type: 'church', id: 'church', areaProperties: new AreaProperties({strokeColor: colors.yellow, fillColor: colors.yellow, icon: m.church})});
            var doctorType = new LocationType({name: "Doctor", type: 'doctor', id: 'doctor', areaProperties: new AreaProperties({strokeColor: colors.orange, fillColor: colors.orange})});
            var hospitalType = new LocationType({name: "Hospital", type: 'hospital', id: 'hospital', areaProperties: new AreaProperties({strokeColor: colors.red, fillColor: colors.red, icon: m.hospital })});
            var liquorType = new LocationType({name: "RUM!!", type: 'liquor_store', id: 'liquor_store', areaProperties: new AreaProperties({strokeColor: colors.maroon, fillColor: colors.maroon})});
            var movieType = new LocationType({name: "Movie Theater", type: 'movie_theater', id: 'movie_theater', areaProperties: new AreaProperties({strokeColor: colors.fuchsia, fillColor: colors.fuchsia})});
            var restaurantType = new LocationType({name: "Restaurant", type: 'restaurant', id: 'restaurant', areaProperties: new AreaProperties({strokeColor: colors.purple, fillColor: colors.purple, icon: m.restaurant})});
            var schoolType = new LocationType({name: "School", type: 'school', id: 'school', areaProperties: new AreaProperties({strokeColor: colors.gray, fillColor: colors.gray, icon: m.university_school })});
            var supermarketType = new LocationType({name: "Supermarket", type: 'grocery_or_supermarket', id: 'grocery_or_supermarket', areaProperties: new AreaProperties({strokeColor:colors.black, fillColor: colors.black})});

            var possibleLocationTypes = new LocationTypes([
                trainStationType,
                airportType,
                embassyType,
                barType,
                bakeryType,
                busStationType,
                churchType,
                doctorType,
                hospitalType,
                liquorType,
                movieType,
                restaurantType,
                schoolType,
                supermarketType
                ]);

            var locationTypesView = new LocationTypesView({
                collection: possibleLocationTypes,
                el: $("#location-types_container")}
            );

            //Custom locations
            var home = new CustomLocation({name: "Home", latitude:"52.1", longitude:"5.1"});
            customLocations = new CustomLocations([home]);

            var customLocationsView = new CustomLocationsView({
                collection: customLocations,
                el: $("#custom-locations-container")
            });

            //Binds autocompletion for default searches.
            //We restrict the search to cities.
            var addressOptions = {
                  types: ['(cities)']
             };

            var addressInput = document.getElementById("address");
            GeoLib.getGeoLib().autocomplete(addressInput, addressOptions);

            // Binds autocompletion for custom searches. No restriction
            var customInput = document.getElementById("custom-address");
            GeoLib.getGeoLib().autocomplete(customInput, null);
        });

        /*
         Sets the current map coordinates to a new value, given in the interface
         */
        function pan() {
            var lat = document.getElementById("set-lat").value;
            var lng = document.getElementById("set-lng").value;

            //Removes input value
            document.getElementById("set-lat").value = "";
            document.getElementById("set-lng").value = "";

            MapsLib.getMapsLib().pan(lat, lng);
        };

        /*
         Sets the current map coordinates to a new value, based on the city input in the interface
         */
        function codeAddress() {

            var resFunction = function panTo(results){
                MapsLib.getMapsLib().pan(results[0].geometry.location.lat(), results[0].geometry.location.lng());
            };

            var errFunction = function error(status){
                console.log("Error while geocoding city " + status);
            };

            var address = document.getElementById('address').value;
            //Removes input value
            document.getElementById('address').value = "";

            // GeoLib.getGeoLib().codeAddress(address);
            GeoLib.getGeoLib().geocode(address, resFunction, errFunction);
        }

        /*
        Adds a new Custom Location to the list.
        */
        function addCustom(){

            var self = this;
            var resFunction = function addMarker(results){
                console.log(results[0].formatted_address);
                var custom = new CustomLocation({
                    name: results[0].formatted_address,
                    latitude:results[0].geometry.location.lat(),
                    longitude:results[0].geometry.location.lng()});
                    customLocations.add(custom);
            };

            var errFunction = function error(status){
                console.log("Error while adding custom location " + status);
            };

            var address = document.getElementById('custom-address').value;

            //Removes input value
            document.getElementById('custom-address').value = "";

            //Add custom location
            console.log("Searching for " + address);
            GeoLib.getGeoLib().geocode(address, resFunction, errFunction);
        }

        $('#goto-submit').click(function() {
            pan();
        });

        $('#address-submit').click(function() {
            codeAddress();
        });

        $('#custom-submit').click(function(){
            addCustom();
        })

   });