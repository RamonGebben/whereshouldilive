
/*
 List of colors. Avoids having to directly use hex values
 FIXME: Do that better
 */
define([], function () {

    var Animate = {

        toggle: function() {
            this.open = !this.open;
            if (this.open) {
                this.toggleOpen()
            } else {
                this.toggleClose()
            };
        },

        toggleMore : function(){
            this.more_open = !this.more_open;
            if (this.more_open) {
                this.toggleMoreOpen()
            } else {
                this.toggleMoreClose()
            };
        },

        open: true,
        more_open: false,

        toggleOpen : function() {
            $('#toggle-symbol').removeClass('fa-angle-left');
            $('#toggle-symbol').addClass('fa-angle-right');
            this.open = true;
            var i = $("#info-bar");
            var t = $("#toggle");
            TweenMax.to(i, 2, { css:{opacity: 1, right: 0}, ease:Expo.easeOut });
            TweenMax.to(t, 2, { css:{right: 0}, ease:Expo.easeOut });
        },

        toggleClose : function() {
            $('#toggle-symbol').removeClass('fa-angle-right');
            $('#toggle-symbol').addClass('fa-angle-left');
            this.open = false;
            var i = $("#info-bar");
            var t = $("#toggle");
            TweenMax.to(i, 2, { css:{ right: -240}, ease:Expo.easeOut });
            TweenMax.to(t, 2, { css:{ right: 0}, ease:Expo.easeOut });
        },

        toggleMoreOpen : function() {
            $('#coordinates-toggle').removeClass('fa-angle-right');
            $('#coordinates-toggle').addClass('fa-angle-down');
            this.more_open = true;
        },

        toggleMoreClose : function() {
            $('#coordinates-toggle').removeClass('fa-angle-down');
            $('#coordinates-toggle').addClass('fa-angle-right');
            this.more_open = false;
        },

        activateToogle : function(){
            var self = this;
            $('#coordinates-toggle').click(function(){
                self.toggleMore()
            });
            $('#toggle').click(function(){
                self.toggle()
            });

        }

//        $(document).ready(function(){
//            activateToogle();
//        })
    };

    return {
        getAnimate: function () { return Animate; }
    };
});