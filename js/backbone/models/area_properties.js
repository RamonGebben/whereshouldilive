/**
 * Created by jll on 21-7-14.
 */
/**
 *   Defines special properties of a circle so that types can have different area properties
 *   (color, shape, . . .)
 */
define([
    '../../utils/colors',
    '../../utils/markers'
    ],
    function (
        Colors,
        Markers) {

    var colors = new Colors();
        var markers = new Markers();

    var AreaProperties = Backbone.Model.extend({
        defaults: {
            icon: markers.more_location,
            strokeColor: colors.red,
            strokeOpacity: 0.01,
            strokeWeight: 2,
            fillColor: colors.red,
            fillOpacity: 0.1
        },

        initialize: function () {
        }
    });

    return AreaProperties;
});