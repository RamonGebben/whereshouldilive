/**
 * Created by Jan Martijn on 10-8-14.
 */
/*
 A Location is a map coordinate set as a Backbone model.
 It has a longitude and a latitude,  but can also have a name.
 */
define([], function () {
    var Location = Backbone.Model.extend({
        defaults:{
            latitude: '0.0',
            longitude: '0.0'
        },

        initialize: function(){
          console.log("New Location created");
        }
    });
    return Location;
});