/**
 * Created by jll on 21-7-14.
 */
/*
 A WhereCoordinate is simply a map coordinate that follows the
 user mouse.
 It extends from the default location.
 */
define([
    'backbone/models/location',
    'maps/leafletMaps'],
    function (Location, MapsLib) {

var WhereCoordinate = Location.extend({
    defaults: {
        map : null
    },
    initialize: function(){
        // console.log("New WhereCoordinate created");
        this.followMap();
    },
    /**
     Adds a mouse listener to display coordinates in real time
     */
    followMap: function(){
        var self = this;
        this.set('map', MapsLib.getMapsLib().map);

        this.get('map').on('mousemove', function(e) {
            var latitude = e.latlng.lat.toFixed(4);
            var longitude = e.latlng.lng.toFixed(4);
            self.set('latitude', latitude);
            self.set('longitude', longitude);
        });
    }

    });
// Current focus point on the map (Coordinate where the mouse currently is located)
//var mainCoordinate = new WhereCoordinate({lat: '52.0802', longitude: '5.1028'});

    return WhereCoordinate;
});
