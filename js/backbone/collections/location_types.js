/**
 * Created by jll on 21-7-14.
 */
/*
 * Collection of LocationTypes.
 */
define(['backbone/models/location_type'], function (LocationType) {
    var LocationTypes = Backbone.Collection.extend({
        initialize: function() {
        },
        model: LocationType
    });

    return LocationTypes;
});