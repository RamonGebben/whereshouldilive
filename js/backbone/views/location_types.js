/**
 * Created by jll on 21-7-14.
 */
/*
 Defines how the collection of location type look like
 */
define(['maps/googleMaps',
        'text!backbone/templates/location_types.html'
        ], function (GeoLib, Template) {

    var LocationTypesView = Backbone.View.extend({
        initialize: function(){
            this.render();
        },
        render: function(){
            var template = _.template(Template);
            this.$el.html(template({
                collection: this.collection.toJSON()
            }));
        },
        events: {
            "click input[type=checkbox]": "activateType"
        },
        /*
         The users indicates one location of interest for him.
         We want to check the results
         */
        activateType: function(event){
            var checkbox = event.currentTarget;

            var id = $(event.currentTarget).data("id");
            var item = this.collection.get(id);
            if(checkbox.checked){
                GeoLib.getGeoLib().performSearch(item);
            }
            else{
                item.hideAreas();
            }
        }
    });
    return LocationTypesView;
});