/**
 * Created by jll on 21-7-14.
 */
/*
 Defines how the current coordinate view looks like
 */
define(['text!backbone/templates/current_coordinate.html'], function (Template) {

    var CurrentCoordinatesView = Backbone.View.extend({
        initialize: function (){
            this.render();
            this.model.on('change', this.render, this); //Adding listener
        },

        render: function () {
            // Compile the template using underscore
            var options = {cur_lat: this.model.get('latitude'), cur_long: this.model.get('longitude')}
            var template = _.template(Template, options);
            // Load the compiled HTML into the Backbone "el"
            this.$el.html(template);
        }
    });

    return CurrentCoordinatesView;
});