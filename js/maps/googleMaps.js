/**
 * Created by jll on 24-7-14.
 */
define(['maps/leafletMaps'//,
//        'async!https://maps.googleapis.com/maps/api/js?key=AIzaSyAF6mxhlF31Au16sEVpfuPZZxLHEEkosLo&libraries=places'
        ],
    function(MapsLib){

        /**
         * Contains all the code related to google maps API.
         * Searches for locations, location types and performs geo coding.
         * @constructor
         */
        var GeoLib ={

            googleMapOptions : {
                center: new google.maps.LatLng(52.0802, 5.1028),
                zoom: 14
            },
            map : null,
            placesService : null,
            geocoder : null,

            init: function(){
                this.map = new google.maps.Map(document.getElementById("map-canvas"), this.googleMapOptions);
                this.placesService = new google.maps.places.PlacesService(this.map);
                this.geocoder = new google.maps.Geocoder();
            },

            /*
                Searches for the input address, and applies resFunction to the result of the
                search.
                If the processing ends up with an error, runs errFunction. with the error status.
            */
            geocode: function(address, resFunction, errFunction){
                this.geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        resFunction(results);
                    } else {
                        errFunction(status);
                    }
                });
            },

            /*
             Sets the current map coordinates to a new value, based on the city input in the interface
             */
            // codeAddress : function(address) {
            //     this.geocoder.geocode( { 'address': address}, function(results, status) {
            //         if (status == google.maps.GeocoderStatus.OK) {
            //             MapsLib.getMapsLib().pan(results[0].geometry.location.lat(), results[0].geometry.location.lng());
            //         } else {
            //             console.log('Geocode was not successful : ' + status);
            //         }
            //     });
            // },

            /*
            Autocompletes an input to help the user find its
            custom location
            */
            autocomplete: function(input, options){

                if(typeof(options)==='undefined'){
                    autocomplete = new google.maps.places.Autocomplete(input);
                }
                else{
                    autocomplete = new google.maps.places.Autocomplete(input, options);
                }
            },

            /*
             Searches for all places of the requested type in the current area.
             When finished, callback is called.
             */
            performSearch : function(item) {
                var mapLib = MapsLib.getMapsLib();
                var self = this;

                var type = item.get('type');

                //FIXME: Why does line 2 not work?
                var bounds = this.convertLeafletToGoogleBounds(mapLib.map);
//                var bounds = this.map.getBounds();

                var request = {
                    bounds: bounds, // Where to search
                    types: [type] // What to search for
                };
                console.log("Selected : " + type);

                this.placesService.radarSearch(request, function (results, status) {
                    self.processRadarResults(results, status, item, mapLib);
                });
            },

            processRadarResults : function(results, status, item, mapLib) {
                if (status != google.maps.places.PlacesServiceStatus.OK) {
                    console.log(status);
                    return;
                }

                console.log("Found " + results.length +  " result!");
                mapLib.createAreasOfInterest(item, results);

//                var areaProperties = item.get('areaProperties');
//                var areaOfInterestList = item.get('areaOfInterestList');
//                for (var i = 0, result; result = results[i]; i++) {
//                    //Create AOI and adds it to the item.
//                    // TODO: Avoid duplicates
//                    var aoi = createAreaOfInterest(result, areaProperties);
//                    areaOfInterestList.add(aoi);
//                }
//                item.set({areaOfInterestList: areaOfInterestList});
//                var aoil = item.get('areaOfInterestList');
//                aoil.display();
            },

            /**
             * Converts the bounds of the input leaflet map to google maps bounds
             * @param leafletMap
             * @returns {o.LatLngBounds}
             *
             */
            convertLeafletToGoogleBounds : function(map){
                //leaflet bounds;
                var lBounds = map.getBounds();
                //    console.log(lBounds);
                var lne = lBounds.getNorthEast();
                var lsw = lBounds.getSouthWest();

                // Google bounds
                var gne = new google.maps.LatLng(lne.lat, lne.lng);
                var gsw = new google.maps.LatLng(lsw.lat, lsw.lng);
                var gBounds = new google.maps.LatLngBounds(gsw, gne);
                return gBounds;
            }
        };

        return {
            getGeoLib: function () { return GeoLib; }
        };
    });

